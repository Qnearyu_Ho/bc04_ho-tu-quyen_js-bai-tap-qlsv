function sinhVien(tenSV, maSV, email, password, diemToan, diemLy, diemHoa){
    this.tenSV = tenSV;
    this.maSV = maSV;
    this.email = email;
    this.password = password;
    this.diemToan = diemToan;
    this.diemLy = diemLy;
    this.diemHoa = diemHoa;
    this.tinhDTB = function(){
        return (this.diemToan + this.diemLy + this.diemHoa) / 3;
    }
}