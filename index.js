const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";
//chức năng thêm sinh viên, tạo ngoài hàm vì nếu tạo trong in ra thì the end

var dssv = [];
// láy thông tin từ localStorage
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
console.log("dssvJson: ", dssvJson);
if (dssvJson != null) {
  console.log("Yes");
  dssv = JSON.parse(dssvJson);
  // array khi convert thanh json se mat function
  for (index = 0; index < dssv.length; index++) {
    var sv = dssv[index];

    dssv[index] = new sinhVien(
      sv.tenSV,
      sv.maSV,
      sv.email,
      sv.password,
      sv.diemToan,
      sv.diemLy,
      sv.diemHoa
    );
  }
}
function themSV() {
  var newSv = layThongTinTuForm();
  // console.log('newSV: ', newSV);
  var isValid =
    validation.kiemTraRong(
      newSv.maSV,
      "spanMaSV",
      "Mã sinh viên không được rỗng"
    ) &&
    // validation.kiemTraRong(
    //   newSv.maSV,
    //   "spanMaSV",
    //   "Mã sinh viên không được rỗng"
    // ) &
    // validation.kiemTraRong(
    //   newSv.email,
    //   "spanEmailSV",
    //   "Email sinh viên không được rỗng"
    // ) &
    // validation.kiemTraRong(
    //   newSv.password,
    //   "spanMatKhau",
    //   "Mật khẩu sinh viên không được rỗng"
    // ) &
    validation.kiemTraDoDai(
      newSv.maSV,
      "spanMaSV",
      "Mã sinh viên phải có 4 kí tự",
      4,
      4
    );
  isValid =
    validation.kiemTraRong(
      newSv.tenSV,
      "spanTenSV",
      "Tên sinh viên không được rỗng"
    ) &&
    validation.kiemTraDoDai(
      newSv.tenSV,
      "spanTenSV",
      "Tên sinh viên phải có 4 kí tự",
      4,
      4
    );
  isValid =
    validation.kiemTraRong(
      newSv.email,
      "spanEmailSV",
      "Email sinh viên không được rỗng"
    ) &&
    validation.kiemTraEmail(
      newSv.email,
      "spanEmailSV",
      "Email phải có cấu trúc abc@gmai.com"
    );
  isValid = validation.kiemTraRong(
    newSv.password,
    "spanMatKhau",
    "Mật khẩu không được rỗng"
  ); 
  // validation.kiemTraDoDai(
  //   newSv.password,
  //   "spanMatKhau",
  //   "Mật khẩu phải có ít nhất 8 kí tự",
  //   8,
  //   8
  // );
  if (isValid) {
    dssv.push(newSv);
    // Create JSON
    var dssvJson = JSON.stringify(dssv);
    // save json into localStorage
    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    // console.log('dssv: ', dssv);
  }
}

function deleteSinhVien(id) {
  // id được truyền vào từ "deleteSinhvien(${sv.maSV})"
  console.log(id);

  // var index = dssv.findIndex(function(sv){
  //     return sv.maSV == id;
  // });
  var index = timKiemViTri(id, dssv);
  // console.log(index);
  if (index !== -1) {
    dssv.splice(index, 1);
    renderDSSV(dssv);
  }
}

function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index !== -(-1)) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
function resetSinhVien() {
  document.getElementById("txtMaSV").reset();
  document.getElementById("txtTenSV").reset();
  document.getElementById("txtEmail").reset();
  document.getElementById("txtPass").reset();

}
